# Hugo Developer Theme
[![License: CC BY 3.0](https://img.shields.io/badge/License-CC%20BY%203.0-lightgrey.svg)](https://creativecommons.org/licenses/by/3.0/)
- Port of Develooper-Theme to Hugo static site generator.
- Original theme developed by Xiaoying Riley
## Author & License
### Adaptation Author
This is an adaptation by Brad Stinson of Xiaoying Riley's Bootstrap Developer-Theme to conform it to Hugo theme standards. The original license is preserved.
### Original Author
This Bootstrap portfolio theme is made by UX/UI designer [Xiaoying Riley](https://twitter.com/3rdwave_themes) for developers and is 100% FREE under the [Creative Commons Attribution 3.0 License (CC BY 3.0)](http://creativecommons.org/licenses/by/3.0/)

If you'd like to **use the template without the attribution**, you can [buy the **commercial license** via the theme website](https://themes.3rdwavemedia.com/bootstrap-templates/resume/free-bootstrap-theme-for-web-developers/)

[![License: CC BY 3.0](https://i.creativecommons.org/l/by/3.0/80x15.png)](http://creativecommons.org/licenses/by/3.0/)
